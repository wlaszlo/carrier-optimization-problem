#!/usr/bin/python3

import json
import sys

file = sys.argv[1]

try:
    with open(file) as json_file:
        data = json.load(json_file)
except:
    print("Could not process the file '{0}'!".format(file))
    sys.exit(1)

orders = sorted(data['orders'], key=lambda x: (x['distance']), reverse=True)
carriers = sorted(data['carriers'], key=lambda x: (x['fixed_cost']))
final_orders = []


def process_order():
    final_orders.append({"carrier": carrier, "order": order})
    orders.remove(order)


for carrier in carriers:
    carrier['delivered'] = 0

    for order in orders[:]:
        if carrier["delivered"] is None:
            process_order()
        elif carrier["delivered"] != carrier["usage_max"]:
            carrier["delivered"] += 1
            process_order()
        else:
            break

results = {
    "final_orders": final_orders,
    "unsolved_orders": orders
}

with open(file.replace('.json', '.solution.json'), 'w') as outfile:
    json.dump(results, outfile, indent=4)
